# SogeBot Updater

Bash script to update SogeBot when using SQLite.
It will download the latest version, backup your SQLite DB and your .env file and extract the file to $HOME/Streaming/bots/sogebot/.

It uses __wget__ and requires __xidel__ [Xidel](https://github.com/benibela/xidel/) to avoid messy, unpredictable regex inside a Bash script. To be less invasive it won't install Xidel. It is by default downloaded to a temporary folder to be used from there. Passing "1" as an argument for __setup.sh__, will install __xidel__ in $HOME/.local/bin.

- https://github.com/GloriousEggroll/proton-ge-custom
- https://github.com/benibela/xidel/

## Getting started

### Downloading

You can download to my default streaming related folder, $HOME/Streaming/bots.

`wget --continue https://gitlab.com/msmafra/sogebotup/-/raw/main/sogebotup?inline=false --output-document=$HOME/.local/bin/sogebotup`

Or you can downlod on via other methods. But if your going to use a diffent folder then $HOME/Streaming/bots, you'll have to change the value of the variables __bots_path__ and maybe __sogetbot_folder_name__ if want to change the final folder name.

You can clone the repository

`git clone https://gitlab.com/msmafra/sogebotup.git`

You can download it to your $HOME/.local/bin to make it available at a terminal for your user.

`wget --continue https://gitlab.com/msmafra/sogebotup/-/raw/main/sogebotup?inline=false --output-document=$HOME/Streaming/bots/bin/sogebotup`
